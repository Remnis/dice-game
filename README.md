# Introduction
The dice game is based on the "Game of Dice Task" by Prof. Dr. Matthias Brand.
You play with a six sided dice (1W6). You can bet of the outcome of the roll. You can not only bet on the direct hit but also on group of possible outcomes (similar to roulette).

# Requirements
jQuery

# Install

# Functions
## setDice(<array>) ##
Sets the available sides of the dice. If empty, a six sided dice is created. Otherwise you can determine which numbers can be drawn.

## animateDice() ##
When property "isDiceAnimated" is set to true, it animates the rolling dice.

## initLog() ##
// TODO

## setBet(<int>) ##
Sets the current bet for the next roll. 

## setMaxRounds(<int>) ##
Sets the maximal playable rounds. 
-1 sets it to unlimited rounds

## logToCsv() ##
Converts the saved data to a CSV format

## isGuessCorrect() ##
Returns if the last guess was correct