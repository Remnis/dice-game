﻿function Dice() {
    // sides of the dice
    this.values = Array();
    // logs
    this.timeSpans = Array();
    // last result from the roll
    this.lastRoll = undefined;
    this.id = 0;
    this.maxRounds = -1;
    this.bet = undefined;
    this.credit = undefined;
    this.guess = Array();
    this.isfalse = false;
    this.isDiceAnimated = false;
    this.animationTime = 150;

    // dice display
    this.virtualDice = Array();
    this.virtualDice[1] = '<img src="https://openclipart.org/image/70px/svg_to_png/220834/1434442078.png&disposition=attachment" alt="Snake Eyes" title="Snake Eyes by  phidari ( https://openclipart.org/user-detail/phidari )" />';
    this.virtualDice[2] = '<img src="https://openclipart.org/image/70px/svg_to_png/220835/1434442095.png&disposition=attachment" alt="Two" title="Two by  phidari ( https://openclipart.org/user-detail/phidari )" />';
    this.virtualDice[3] = '<img src="https://openclipart.org/image/70px/svg_to_png/220836/1434442108.png&disposition=attachment" alt="Three" title="Three by  phidari ( https://openclipart.org/user-detail/phidari )" />';
    this.virtualDice[4] = '<img src="https://openclipart.org/image/70px/svg_to_png/220837/1434442120.png&disposition=attachment" alt="Four" title="Four by  phidari ( https://openclipart.org/user-detail/phidari )" />';
    this.virtualDice[5] = '<img src="https://openclipart.org/image/70px/svg_to_png/220838/1434442131.png&disposition=attachment" alt="Five" title="Five by  phidari ( https://openclipart.org/user-detail/phidari )" />';
    this.virtualDice[6] = '<img src="https://openclipart.org/image/70px/svg_to_png/220839/1434442143.png&disposition=attachment" alt="Six" title="Six by  phidari ( https://openclipart.org/user-detail/phidari )" />';

    // constructor
    this.constructor = function (args) {
        if (args != undefined) {
            this.isfalse = true;
        }
        this.setDice();
        this.credit = 1000;
        this.refreshDisplay();
        this.initLog();
    }

    this.setDice = function (args) {
        this.values = Array();
        if (args == undefined) {
            // default, six-side dice
            for (var i = 1; i <= 6; i++) {
                this.values.push(i);
            }
        }
        else {
            // custom, always loose
            for (var i = 1; i <= 6; i++) {
                if (args.indexOf(i) < 0) {
                    this.values.push(i);
                }
            }
        }
    }

    this.animateDice = function () {
        var that = this;
        if(!this.isDiceAnimated){
            return;
        }
        var diceN = 1;
        var inter = window.setInterval(function () {
            $('[data-dicetype="dice"]').html(that.virtualDice[diceN]);
            diceN++;
            console.log(new Date(), diceN);
            if (diceN > 6) {
                $('[data-dicetype="dice"]').html(that.virtualDice[that.lastRoll]);
                window.clearInterval(inter);
            }
        }, this.animationTime);

        

    }

    this.initLog = function(){
        this.timeSpans.push(["round", "date", "dice", "bet", "credit", "guessed", "guess correct", "maxrounds", "isfakeorreal"]);
    }

    // roll the dice
    this.roll = function () {
        //test, if a bet is set
        if (!this.getBet()) {
            this.error("Please place your bet first.");
            return false;
        }
        // max rounds exceeded
        else if (this.getMaxRounds() > 0 && this.getMaxRounds() <= this.id) {
            this.error("Maximale Rundenanzahl erreicht.\nEndergebnis: " + this.getCredit() + " Euro");
            return false;
        }
        var rnd = Math.floor(Math.random() * this.values.length);
        this.lastRoll = this.values[rnd];
        this.id++;
        if (this.isGuessCorrect()) {
            // add money
            this.addCredit(this.getBet());
            $('[data-dicetype="winloose"]').html(this.getBet() + " &euro;");
        }
        else {
            // remove money
            this.addCredit(this.getBet() * -1);
            $('[data-dicetype="winloose"]').html((this.getBet() * -1) + " &euro;");
        }
        // fill log
        //this.timeSpans.push([this.id, (new Date()).toJSON(), this.lastRoll, this.getBet(), this.getCredit(), "\"" + this.getGuess().join(",") + "\"", this.isGuessCorrect(), this.getMaxRounds(), (this.isfalse ? "fake" : "real")]);
        this.timeSpans.push([this.id, this.formatDate(new Date()), this.lastRoll, this.getBet(), this.getCredit(), "\"" + this.getGuess().join(",") + "\"", this.isGuessCorrect(), this.getMaxRounds(), (this.isfalse ? "fake" : "real")]);
        // refresh pocket
        
        if (this.lastRoll) {
            this.animateDice();
        }
        this.refreshDisplay();
        $('[data-dicetype="dice"]').html(this.virtualDice[this.lastRoll]);
        return this.lastRoll;
    }

    this.refreshDisplay = function(){
        $('[data-dicetype="money"]').html(this.getCredit() + " &euro;");
        $('[data-dicetype="bet"]').html(this.getBet() + " &euro;");
        
        if (this.getMaxRounds() > 0) {
            $('[data-dicetype="round"]').html(this.id + " / " + this.getMaxRounds());
        }
        else{
            $('[data-dicetype="round"]').html(this.id);
        }
        // logging
        var logContainer = $('[data-dicetype="log"]');
        if (logContainer.length > 0) {
            if ($(logContainer).is("input")) {
                $(logContainer).val(this.logToCsv());
            }
            else if ($(logContainer).is("textarea")) {
                $(logContainer).text(this.logToCsv());
            }
            else {
                $(logContainer).html(this.logToHtml());
            }
        }
        
    }

    this.setBet = function(m){
        this.bet = m;
    }

    this.getBet = function(){
        return this.bet;
    }

    this.addCredit = function(m){
        this.credit += m;
    }

    this.getCredit = function () {
        return this.credit;
    }

    this.setGuess = function (gs) {
        this.guess = Array();
        for (var i = 0; i < gs.length; i++) {
            this.guess.push(gs[i]);
        }
    }

    this.getGuess = function () {
        return this.guess;
    }

    this.isGuessCorrect = function () {
        return this.getGuess().indexOf(this.lastRoll) > 0;
    }

    this.logToCsv = function () {
        var str = "";
        for (var i = 0; i < this.timeSpans.length; i++) {
            str += this.timeSpans[i].join(",") + "\n";
        }
        return str;
    }

    this.logToHtml = function () {
        var str = "";
        for (var i = 0; i < this.timeSpans.length; i++) {
            str += this.timeSpans[i].join(",") + "<br/>";
        }
        return str;
    }

    this.setMaxRounds = function (rounds) {
        this.maxRounds = parseInt(rounds, 10);
    }

    this.getMaxRounds = function () {
        return this.maxRounds
    }

    // init
    if(arguments.length > 0){
        this.constructor(arguments);
    }
    else {
        this.constructor();
    }

    this.error = function () {
        alert(arguments[0]);
    }

    this.formatDate = function (date) {
        var dt = "";
        /*
        dt = date.getFullYear();
        dt += "-";
        dt += date.getMonth() < 9 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);
        dt += "-";
        dt += date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        */
        dt = date.toJSON();
        return dt;
    }

    this.init = function () {
        var that = this;
        $('[data-dice]').on("click", function () {
            // reset css
            $('[data-dice]').removeClass("enabled");
            $(this).addClass("enabled");
            // dice values
            var val = $(this).data("dice");
            var vals = ("" + val).split(",");
            var intVals = Array();
            for (var i = 0; i < vals.length; i++) {
                intVals.push(parseInt(vals[i], 10));
            }
            that.setGuess(intVals);
            if (that.isfalse) {
                that.setDice(intVals);
            }
            else {
                that.setDice();
            }
            // bet
            var b = $(this).data("bet");
            that.setBet(parseInt(b, 10));
            that.refreshDisplay();
        });
    }
}